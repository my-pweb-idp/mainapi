package com.acs.pweb.controller.impl;

import com.acs.pweb.controller.ProductController;
import com.acs.pweb.controller.ProfileController;
import com.acs.pweb.domain.Response;
import com.acs.pweb.domain.request.ProfileDTO;
import com.acs.pweb.model.Profile;
import com.acs.pweb.service.FilesStorageService;
import com.acs.pweb.service.ProfileService;
import com.acs.pweb.service.impl.RabbitMQSender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@WebMvcTest(controllers = ProfileController.class)
@ExtendWith(SpringExtension.class)
class ProfileControllerImplTest {

    @MockBean
    private FilesStorageService storageService;

    @MockBean
    private RabbitMQSender rabbitMQSender;

    @MockBean
    private ProfileService profileService;

    @Autowired
    private ProfileControllerImpl controller;

    @BeforeEach
    void initUseCase(){
        controller = new ProfileControllerImpl(profileService);
    }

    @Test
    void addProfileWithSuccess() throws IOException {
        ProfileDTO profileDTO = new ProfileDTO();
        profileDTO.setPhoneNumber("2313213");
        profileDTO.setName("Sergiu");
        profileDTO.setEmail("S@gmail.com");

        Profile profile = new Profile();
        profile.setId(1);
        profile.setEmail("S@gmail.com");
        profile.setName("Sergiu");

        Response response = new Response();
        response.setStatus(200);
        response.setMessage("SUCCESS");


        Mockito.when(profileService.savaOrUpdateProfile(profileDTO)).thenReturn(response);

        ResponseEntity<Response> result = controller.saveOrUpdateProfile(profileDTO);

        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void updateWithSuccess() throws IOException {
        ProfileDTO profileDTO = new ProfileDTO();
        profileDTO.setPhoneNumber("2313213");
        profileDTO.setName("Sergiu");
        profileDTO.setEmail("S@gmail.com");

        Profile profile = new Profile();
        profile.setId(1);
        profile.setEmail("S@gmail.com");
        profile.setName("Sergiu");

        Response response = new Response();
        response.setStatus(200);
        response.setMessage("SUCCESS");


        Mockito.when(profileService.updateProfile(profileDTO)).thenReturn(response);

        ResponseEntity<Response> result = controller.updateProfile(profileDTO);

        assertEquals(200, result.getStatusCodeValue());
    }


    @Test
    void getProfileWithSuccess() throws IOException {
        ProfileDTO profileDTO = new ProfileDTO();
        profileDTO.setPhoneNumber("2313213");
        profileDTO.setName("Sergiu");
        profileDTO.setEmail("S@gmail.com");

        Profile profile = new Profile();
        profile.setId(1);
        profile.setEmail("S@gmail.com");
        profile.setName("Sergiu");

        Response response = new Response();
        response.setStatus(200);
        response.setMessage("SUCCESS");


        Mockito.when(profileService.getProfile(profileDTO.getEmail())).thenReturn(profileDTO);

        ResponseEntity<ProfileDTO> result = controller.getProfile(profileDTO.getEmail());

        assertEquals(200, result.getStatusCodeValue());
    }
}
