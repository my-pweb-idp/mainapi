package com.acs.pweb.controller.impl;


import com.acs.pweb.controller.ProductController;
import com.acs.pweb.domain.Response;
import com.acs.pweb.domain.request.ProductDTO;
import com.acs.pweb.domain.response.*;
import com.acs.pweb.service.FilesStorageService;
import com.acs.pweb.service.ProductService;
import com.acs.pweb.service.impl.RabbitMQSender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@WebMvcTest(controllers = ProductController.class)
@ExtendWith(SpringExtension.class)
class ProductControllerImplTest {

    @MockBean
    private ProductService productService;

    @MockBean
    private FilesStorageService storageService;

    @MockBean
    private RabbitMQSender rabbitMQSender;

    @Autowired
    private ProductControllerImpl controller;


    @BeforeEach
    void initUseCase() {
        controller = new ProductControllerImpl(productService, storageService);
    }

    @Test
    void addProductWithSuccess() {
        ProductDTO product = new ProductDTO();
        product.setTitle("Ana");
        product.setEmail("sergiu@gmail.com");
        product.setDescription("ewqeqweqwe");

        Response response = new Response();
        response.setStatus(200);
        response.setMessage("SUCCESS");
        response.setResult("Dsa");

        Mockito.when(productService.postProduct(product))
                .thenReturn(response);

        ResponseEntity<Response> result = controller.postProduct(product);

        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void updateProductWithSuccess() throws IOException {
        ProductDTO product = new ProductDTO();
        product.setTitle("Ana");
        product.setEmail("sergiu@gmail.com");
        product.setDescription("ewqeqweqwe");

        Response response = new Response();
        response.setStatus(200);
        response.setMessage("SUCCESS");
        response.setResult("Dsa");

        Mockito.when(productService.updateProduct(product, 1))
                .thenReturn(response);

        ResponseEntity<Response> result = controller.updateProduct(product, 1);

        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void deleteProductWithSuccess() throws IOException {
        Response response = new Response();
        response.setStatus(200);
        response.setMessage("SUCCESS");
        response.setResult("Dsa");

        Mockito.when(productService.deleteProduct(1))
                .thenReturn(response);

        ResponseEntity<Response> result = controller.deleteProduct(1);

        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void getProductWithSuccess() throws IOException {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setTitle("Ana");
        productDTO.setEmail("sergiu@gmail.com");
        productDTO.setDescription("ewqeqweqwe");

        ProductResp product = new ProductResp();
        product.setTitle("Ana");
        product.setLocationLong("1.1111");
        product.setDescription("ewqeqweqwe");

        Response response = new Response();
        response.setStatus(200);
        response.setMessage("SUCCESS");
        response.setResult("Dsa");

        Mockito.when(productService.getProduct(1))
                .thenReturn(product);

        ResponseEntity<Response> result = controller.postProduct(productDTO);

        assertEquals(200, result.getStatusCodeValue());
    }


    @Test
    void responseToRequestWithSuccess() throws IOException {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setTitle("Ana");
        productDTO.setEmail("sergiu@gmail.com");
        productDTO.setDescription("ewqeqweqwe");

        ProductResp product = new ProductResp();
        product.setTitle("Ana");
        product.setLocationLong("1.1111");
        product.setDescription("ewqeqweqwe");

        Response response = new Response();
        response.setStatus(200);
        response.setMessage("SUCCESS");
        response.setResult("Dsa");

        Mockito.when(productService.responseToRequest(productDTO.getEmail(), product.getId(), "comment", 1))
                .thenReturn(response);

        ResponseEntity<Response> result = controller.responseToRequest(productDTO.getEmail(), product.getId(), "comment", 1);

        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void postImageWithSuccess() throws IOException {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setTitle("Ana");
        productDTO.setEmail("sergiu@gmail.com");
        productDTO.setDescription("ewqeqweqwe");

        ProductResp product = new ProductResp();
        product.setTitle("Ana");
        product.setLocationLong("1.1111");
        product.setDescription("ewqeqweqwe");

        Response response = new Response();
        response.setStatus(200);
        response.setMessage("SUCCESS");
        response.setResult("Dsa");

        Mockito.when(productService.postImage(productDTO.getEmail(), product.getTitle(), null))
                .thenReturn(response);

        ResponseEntity<Response> result = controller.postImage(productDTO.getEmail(), product.getTitle(), null);

        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void addToFavoriteWithSuccess() throws IOException {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setTitle("Ana");
        productDTO.setEmail("sergiu@gmail.com");
        productDTO.setDescription("ewqeqweqwe");

        ProductResp product = new ProductResp();
        product.setTitle("Ana");
        product.setLocationLong("1.1111");
        product.setDescription("ewqeqweqwe");

        Response response = new Response();
        response.setStatus(200);
        response.setMessage("SUCCESS");
        response.setResult("Dsa");

        Mockito.when(productService.addToFavorite(productDTO.getEmail(), product.getId()))
                .thenReturn(response);

        ResponseEntity<Response> result = controller.addToFavorite(productDTO.getEmail(), product.getId());

        assertEquals(200, result.getStatusCodeValue());
    }


    @Test
    void deleteFromFavoriteWithSuccess() throws IOException {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setTitle("Ana");
        productDTO.setEmail("sergiu@gmail.com");
        productDTO.setDescription("ewqeqweqwe");

        ProductResp product = new ProductResp();
        product.setTitle("Ana");
        product.setLocationLong("1.1111");
        product.setDescription("ewqeqweqwe");

        Response response = new Response();
        response.setStatus(200);
        response.setMessage("SUCCESS");
        response.setResult("Dsa");

        Mockito.when(productService.deleteFromFavorite(productDTO.getEmail(), product.getId()))
                .thenReturn(response);

        ResponseEntity<Response> result = controller.deleteFromFavorite(productDTO.getEmail(), product.getId());

        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void getFavoriteWithSuccess() throws IOException {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setTitle("Ana");
        productDTO.setEmail("sergiu@gmail.com");
        productDTO.setDescription("ewqeqweqwe");

        ProductResp product = new ProductResp();
        product.setTitle("Ana");
        product.setLocationLong("1.1111");
        product.setDescription("ewqeqweqwe");

        Response response = new Response();
        response.setStatus(200);
        response.setMessage("SUCCESS");
        response.setResult("Dsa");

        Mockito.when(productService.getFavorite(productDTO.getEmail(), product.getId()))
                .thenReturn(response);

        ResponseEntity<Response> result = controller.getFavorite(productDTO.getEmail(), product.getId());

        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void getAllProductsWithSuccess() throws IOException {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setTitle("Ana");
        productDTO.setEmail("sergiu@gmail.com");
        productDTO.setDescription("ewqeqweqwe");

        ProductResp product = new ProductResp();
        product.setTitle("Ana");
        product.setLocationLong("1.1111");
        product.setDescription("ewqeqweqwe");

        PageableContent response = new PageableContent();

        Mockito.when(productService.getAllProducts(null, 3, 2))
                .thenReturn(response);

        ResponseEntity<PageableContent> result = controller.getAllProducts(null, 3, 2);

        assertEquals(200, result.getStatusCodeValue());
    }


    @Test
    void getUserFavoriteProductsWithSuccess() throws IOException {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setTitle("Ana");
        productDTO.setEmail("sergiu@gmail.com");
        productDTO.setDescription("ewqeqweqwe");

        ProductResp product = new ProductResp();
        product.setTitle("Ana");
        product.setLocationLong("1.1111");
        product.setDescription("ewqeqweqwe");

        List<ProductResponse> response = new ArrayList<>();


        Mockito.when(productService.getUserFavoriteProducts(null))
                .thenReturn(response);

        ResponseEntity<List<ProductResponse>> result = controller.getUserFavoriteProducts(null);

        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void getUserProductsWithSuccess() throws IOException {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setTitle("Ana");
        productDTO.setEmail("sergiu@gmail.com");
        productDTO.setDescription("ewqeqweqwe");

        ProductResp product = new ProductResp();
        product.setTitle("Ana");
        product.setLocationLong("1.1111");
        product.setDescription("ewqeqweqwe");

        List<ProductResponse> response = new ArrayList<>();


        Mockito.when(productService.getUserProducts(null))
                .thenReturn(response);

        ResponseEntity<List<ProductResponse>> result = controller.getUserProducts(null);

        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void getTypesWithSuccess() throws IOException {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setTitle("Ana");
        productDTO.setEmail("sergiu@gmail.com");
        productDTO.setDescription("ewqeqweqwe");

        ProductResp product = new ProductResp();
        product.setTitle("Ana");
        product.setLocationLong("1.1111");
        product.setDescription("ewqeqweqwe");

        List<TypeResponse> response = new ArrayList<>();


        Mockito.when(productService.getTypes())
                .thenReturn(response);

        ResponseEntity<List<TypeResponse>> result = controller.getTypes();

        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void myRequestsWithSuccess() throws IOException {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setTitle("Ana");
        productDTO.setEmail("sergiu@gmail.com");
        productDTO.setDescription("ewqeqweqwe");

        ProductResp product = new ProductResp();
        product.setTitle("Ana");
        product.setLocationLong("1.1111");
        product.setDescription("ewqeqweqwe");

        List<RequestsResponse> response = new ArrayList<>();


        Mockito.when(productService.myRequests(productDTO.getEmail()))
                .thenReturn(response);

        ResponseEntity<List<RequestsResponse>> result = controller.myRequests(productDTO.getEmail());

        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void myGivingRequestsWithSuccess() throws IOException {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setTitle("Ana");
        productDTO.setEmail("sergiu@gmail.com");
        productDTO.setDescription("ewqeqweqwe");

        ProductResp product = new ProductResp();
        product.setTitle("Ana");
        product.setLocationLong("1.1111");
        product.setDescription("ewqeqweqwe");

        List<RequestsResponse> response = new ArrayList<>();


        Mockito.when(productService.myGivingRequests(productDTO.getEmail()))
                .thenReturn(response);

        ResponseEntity<List<RequestsResponse>> result = controller.myGivingRequests(productDTO.getEmail());

        assertEquals(200, result.getStatusCodeValue());
    }
}
