package com.acs.pweb;

import com.acs.pweb.service.FilesStorageService;
import com.acs.pweb.service.impl.RabbitMQSender;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
class PwebApplicationTests {

	@MockBean
	private FilesStorageService storageService;

	@MockBean
	private RabbitMQSender rabbitMQSender;

	@Test
	void contextLoads() {
	}

}
