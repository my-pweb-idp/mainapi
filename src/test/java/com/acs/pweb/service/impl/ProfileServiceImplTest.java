package com.acs.pweb.service.impl;

import com.acs.pweb.domain.Response;
import com.acs.pweb.domain.request.ProfileDTO;
import com.acs.pweb.model.Location;
import com.acs.pweb.model.Profile;
import com.acs.pweb.repository.LocationRepository;
import com.acs.pweb.repository.ProfileRepository;
import com.acs.pweb.service.ProfileService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class ProfileServiceImplTest {
    @Mock
    private ProfileRepository profileRepository;
    @Mock
    private LocationRepository locationRepository;
    @Mock
    private AsyncNotificationServiceImpl asyncNotificationService;

    @Autowired
    private ProfileService profileService;

    @BeforeEach
    void initUseCase() {
        profileService = new ProfileServiceImpl(profileRepository, locationRepository, asyncNotificationService);
    }

    @Test
    void addProfileLocationNullWithSuccess() throws IOException {
        ProfileDTO profileDTO = new ProfileDTO();
        profileDTO.setPhoneNumber("2313213");
        profileDTO.setName("Sergiu");
        profileDTO.setEmail("S@gmail.com");

        Profile profile = new Profile();
        profile.setId(1);
        profile.setEmail("S@gmail.com");
        profile.setName("Sergiu");

        Mockito.when(profileRepository.getProfileByEmail(profileDTO.getEmail())).thenReturn(profile);

        Mockito.when(profileRepository.save(profile)).thenReturn(profile);

        Response result = profileService.savaOrUpdateProfile(profileDTO);

        assertEquals(200, result.getStatus());
    }

    @Test
    void addProfileLocationNotNullWithSuccess() throws IOException {
        ProfileDTO profileDTO = new ProfileDTO();
        profileDTO.setPhoneNumber("2313213");
        profileDTO.setName("Sergiu");
        profileDTO.setEmail("S@gmail.com");
        profileDTO.setLocationLat("1.11");
        profileDTO.setLocationLong("1.1111");

        Profile profile = new Profile();
        profile.setId(1);
        profile.setEmail("S@gmail.com");
        profile.setName("Sergiu");

        Location location = new Location();
        location.setLongitude(profileDTO.getLocationLong());
        location.setLatitude(profileDTO.getLocationLat());

        Mockito.when(profileRepository.getProfileByEmail(profileDTO.getEmail())).thenReturn(profile);

        Mockito.when(locationRepository.save(location)).thenReturn(location);

        Mockito.when(profileRepository.save(profile)).thenReturn(profile);

        Response result = profileService.savaOrUpdateProfile(profileDTO);

        assertEquals(200, result.getStatus());
    }

    @Test
    void addProfileNameNullWithSuccess() throws IOException {
        ProfileDTO profileDTO = new ProfileDTO();
        profileDTO.setPhoneNumber("2313213");
        profileDTO.setEmail("S@gmail.com");
        profileDTO.setLocationLat("1.11");
        profileDTO.setLocationLong("1.1111");

        Profile profile = new Profile();
        profile.setId(1);
        profile.setEmail("S@gmail.com");
        profile.setName("Sergiu");

        Location location = new Location();
        location.setLongitude(profileDTO.getLocationLong());
        location.setLatitude(profileDTO.getLocationLat());

        Mockito.when(profileRepository.getProfileByEmail(profileDTO.getEmail())).thenReturn(profile);

        Mockito.when(locationRepository.save(location)).thenReturn(location);

        Mockito.when(profileRepository.save(profile)).thenReturn(profile);

        Response result = profileService.savaOrUpdateProfile(profileDTO);

        assertEquals(200, result.getStatus());
    }

    @Test
    void updateProfileWithSuccess() throws IOException {
        ProfileDTO profileDTO = new ProfileDTO();
        profileDTO.setPhoneNumber("2313213");
        profileDTO.setEmail("S@gmail.com");
        profileDTO.setLocationLat("1.11");
        profileDTO.setLocationLong("1.1111");

        Profile profile = new Profile();
        profile.setId(1);
        profile.setEmail("S@gmail.com");
        profile.setName("Sergiu");

        Location location = new Location();
        location.setLongitude(profileDTO.getLocationLong());
        location.setLatitude(profileDTO.getLocationLat());

        Mockito.when(profileRepository.getProfileByEmail(profileDTO.getEmail())).thenReturn(profile);

        Mockito.when(locationRepository.save(location)).thenReturn(location);

        Mockito.when(profileRepository.save(profile)).thenReturn(profile);

        Response result = profileService.savaOrUpdateProfile(profileDTO);

        assertEquals(200, result.getStatus());
    }

    @Test
    void updateLocationNotNullWithSuccess() throws IOException {
        ProfileDTO profileDTO = new ProfileDTO();
        profileDTO.setPhoneNumber("2313213");
        profileDTO.setName("Sergiu");
        profileDTO.setEmail("S@gmail.com");
        profileDTO.setLocationLat("1.11");
        profileDTO.setLocationLong("1.1111");

        Profile profile = new Profile();
        profile.setId(1);
        profile.setEmail("S@gmail.com");
        profile.setName("Sergiu");

        Location location = new Location();
        location.setLongitude(profileDTO.getLocationLong());
        location.setLatitude(profileDTO.getLocationLat());

        Mockito.when(profileRepository.getProfileByEmail(profileDTO.getEmail())).thenReturn(profile);

        Mockito.when(locationRepository.save(location)).thenReturn(location);

        Mockito.when(profileRepository.save(profile)).thenReturn(profile);

        Response result = profileService.savaOrUpdateProfile(profileDTO);

        assertEquals(200, result.getStatus());
    }

    @Test
    void updateLocationNullWithSuccess() throws IOException {
        ProfileDTO profileDTO = new ProfileDTO();
        profileDTO.setPhoneNumber("2313213");
        profileDTO.setName("Sergiu");
        profileDTO.setEmail("S@gmail.com");

        Profile profile = new Profile();
        profile.setId(1);
        profile.setEmail("S@gmail.com");
        profile.setName("Sergiu");

        Mockito.when(profileRepository.getProfileByEmail(profileDTO.getEmail())).thenReturn(profile);

        Mockito.when(profileRepository.save(profile)).thenReturn(profile);

        Response result = profileService.savaOrUpdateProfile(profileDTO);

        assertEquals(200, result.getStatus());
    }

    @Test
    void getProfileWithSuccess() throws IOException {
        ProfileDTO profileDTO = new ProfileDTO();
        profileDTO.setPhoneNumber("2313213");
        profileDTO.setName("Sergiu");
        profileDTO.setEmail("S@gmail.com");

        Profile profile = new Profile();
        profile.setId(1);
        profile.setEmail("S@gmail.com");
        profile.setName("Sergiu");

        Mockito.when(profileRepository.getProfileByEmail(profileDTO.getEmail())).thenReturn(profile);

        ProfileDTO result = profileService.getProfile(profileDTO.getEmail());

        assertEquals(profileDTO.getEmail(), result.getEmail());
    }
}
