package com.acs.pweb;

import com.acs.pweb.email.EmailRequest;
import com.acs.pweb.service.FilesStorageService;
import com.acs.pweb.service.impl.RabbitMQSender;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import javax.annotation.Resource;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
//@EnableEurekaClient
//@EnableFeignClients
//@EnableAsync
@EnableRabbit
public class PwebApplication implements CommandLineRunner {

	@Resource
	FilesStorageService storageService;

	@Resource
	RabbitMQSender rabbitMQSender;

	public static void main(String[] args) {
		SpringApplication.run(PwebApplication.class, args);
	}

	@Override
	public void run(String... arg) throws Exception {
		EmailRequest emailRequest = new EmailRequest();
		emailRequest.setEmails(Collections.singletonList("sergiubabin17@gmail.com"));
		emailRequest.setCcEmails(Collections.singletonList("nicoleta.sima3599@gmail.com"));
		emailRequest.setTemplateName("ADD_BONUS_DAY");
		emailRequest.setContentType("html");
		emailRequest.setSubject("Lavinia is the best");
		Map<String, String> data = new HashMap<>();
		data.put("noOfBonusDays", "Labaginia");
		emailRequest.setData(data);
		rabbitMQSender.send(emailRequest);
		storageService.init();
	}

}
