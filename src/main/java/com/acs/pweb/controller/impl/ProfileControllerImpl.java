package com.acs.pweb.controller.impl;

import com.acs.pweb.controller.ProfileController;
import com.acs.pweb.domain.Response;
import com.acs.pweb.domain.request.ProfileDTO;
import com.acs.pweb.service.ProfileService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class ProfileControllerImpl implements ProfileController {

    private final ProfileService profileService;

    public ProfileControllerImpl(ProfileService profileService) {
        this.profileService = profileService;
    }

    @Override
    public ResponseEntity<Response> saveOrUpdateProfile(ProfileDTO profileDTO) throws IOException {
        return ResponseEntity.ok().body(profileService.savaOrUpdateProfile(profileDTO));
    }

    @Override
    public ResponseEntity<Response> updateProfile(ProfileDTO profileDTO) throws IOException {
        return ResponseEntity.ok().body(profileService.updateProfile(profileDTO));
    }

    @Override
    public ResponseEntity<ProfileDTO> getProfile(String email) throws IOException {
        return ResponseEntity.ok().body(profileService.getProfile(email));
    }
}
