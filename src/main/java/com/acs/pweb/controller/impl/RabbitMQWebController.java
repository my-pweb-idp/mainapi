package com.acs.pweb.controller.impl;

import com.acs.pweb.email.EmailRequest;
import com.acs.pweb.service.impl.RabbitMQSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/javainuse-rabbitmq/")
public class RabbitMQWebController {

	@Autowired
	RabbitMQSender rabbitMQSender;

	@GetMapping(value = "/producer")
	public String producer(@RequestParam("empName") String empName,@RequestParam("empId") String empId) {

		EmailRequest emailRequest = new EmailRequest();
		emailRequest.setEmails(Collections.singletonList("sergiubabin17@gmail.com"));
		emailRequest.setCcEmails(Collections.singletonList("nicoleta.sima3599@gmail.com"));
		emailRequest.setTemplateName("ADD_BONUS_DAY");
		emailRequest.setContentType("html");
		emailRequest.setSubject("Lavinia is the best");
		Map<String, String> data = new HashMap<>();
		data.put("noOfBonusDays", "Labaginia");
		emailRequest.setData(data);
		rabbitMQSender.send(emailRequest);

		return "Message sent to the RabbitMQ JavaInUse Successfully";
	}

}

