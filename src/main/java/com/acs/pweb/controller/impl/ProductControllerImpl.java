package com.acs.pweb.controller.impl;

import com.acs.pweb.controller.ProductController;
import com.acs.pweb.domain.Response;
import com.acs.pweb.domain.request.ProductDTO;
import com.acs.pweb.domain.response.*;
import com.acs.pweb.service.FilesStorageService;
import com.acs.pweb.service.ProductService;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class ProductControllerImpl implements ProductController {

    private final ProductService productService;
    private final FilesStorageService filesStorageService;

    public ProductControllerImpl(ProductService productService, FilesStorageService filesStorageService) {
        this.productService = productService;
        this.filesStorageService = filesStorageService;
    }

    @Override
    public ResponseEntity<Response> postImage(String email, String product, MultipartFile file) {
        return ResponseEntity.ok().body(productService.postImage(email, product, file));
    }

    @Override
    public ResponseEntity<Response> postProduct(ProductDTO productDTO) {
        return ResponseEntity.ok().body(productService.postProduct(productDTO));
    }

    @Override
    public ResponseEntity<Response> deleteProduct(Integer productId) {
        return ResponseEntity.ok().body(productService.deleteProduct(productId));
    }

    @Override
    public ResponseEntity<Response> updateProduct(ProductDTO productDTO, Integer id) throws IOException {
        return ResponseEntity.ok().body(productService.updateProduct(productDTO, id));
    }

    @Override
    public ResponseEntity<Response> addToFavorite(String email, Integer id) throws IOException {
        return ResponseEntity.ok().body(productService.addToFavorite(email, id));
    }

    @Override
    public ResponseEntity<Response> deleteFromFavorite(String email, Integer id) throws IOException {
        return ResponseEntity.ok().body(productService.deleteFromFavorite(email, id));
    }

    @Override
    public ResponseEntity<Response> getFavorite(String email, Integer id) throws IOException {
        return ResponseEntity.ok().body(productService.getFavorite(email, id));

    }

    @Override
    public ResponseEntity<PageableContent> getAllProducts(String title, int page, int size) throws IOException {
        if (page != 0) page -= 1;
        return ResponseEntity.ok().body(productService.getAllProducts(title, page, size));
    }

    @Override
    public ResponseEntity<List<ProductResponse>> getUserProducts(String userEmail) throws IOException {
        return ResponseEntity.ok().body(productService.getUserProducts(userEmail));
    }

    @Override
    public ResponseEntity<List<ProductResponse>> getUserFavoriteProducts(String userEmail) throws IOException {
        return ResponseEntity.ok().body(productService.getUserFavoriteProducts(userEmail));
    }

    @Override
    public ResponseEntity<ProductResp> getProduct(Integer productId) throws IOException {
        return ResponseEntity.ok().body(productService.getProduct(productId));
    }

    @Override
    public ResponseEntity<List<TypeResponse>> getTypes() throws IOException {
        return ResponseEntity.ok().body(productService.getTypes());
    }

    @Override
    public ResponseEntity<Resource> getFile(@PathVariable String filename, @PathVariable String filePath) {
        Resource file = filesStorageService.load(filename, filePath);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @Override
    public ResponseEntity<Response> requestBook(String userEmail, Integer id) throws IOException {
        return ResponseEntity.ok().body(productService.requestBook(userEmail, id));
    }

    @Override
    public ResponseEntity<Response> responseToRequest(String userEmail, Integer id, String comment, Integer action) throws IOException {
        return ResponseEntity.ok().body(productService.responseToRequest(userEmail, id, comment, action));
    }

    @Override
    public ResponseEntity<List<RequestsResponse>> myRequests(String userEmail) throws IOException {
        return ResponseEntity.ok().body(productService.myRequests(userEmail));
    }

    @Override
    public ResponseEntity<List<RequestsResponse>> myGivingRequests(String userEmail) throws IOException {
        return ResponseEntity.ok().body(productService.myGivingRequests(userEmail));
    }
}
