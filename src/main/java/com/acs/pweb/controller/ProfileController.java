package com.acs.pweb.controller;

import com.acs.pweb.domain.Response;
import com.acs.pweb.domain.request.ProductDTO;
import com.acs.pweb.domain.request.ProfileDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ProfileController {
    @PostMapping("/saveOrUpdateProfile")
    ResponseEntity<Response> saveOrUpdateProfile(@RequestBody ProfileDTO profileDTO) throws IOException;


    @PostMapping("/updateProfile")
    ResponseEntity<Response> updateProfile(@RequestBody ProfileDTO profileDTO) throws IOException;

    @GetMapping("/profile")
    ResponseEntity<ProfileDTO> getProfile(@RequestParam String email) throws IOException;
}
