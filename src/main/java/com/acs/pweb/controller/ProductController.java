package com.acs.pweb.controller;

import com.acs.pweb.domain.Response;

import com.acs.pweb.domain.request.ProductDTO;
import com.acs.pweb.domain.response.*;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface ProductController {
    @PostMapping("/postImage")
    ResponseEntity<Response> postImage(@RequestParam(value = "email") String email, @RequestParam(value = "product") String product, @RequestParam(value = "files", required = false) MultipartFile file);

    @PostMapping("/postProduct")
    ResponseEntity<Response> postProduct(@RequestBody ProductDTO productDTO);

    @DeleteMapping("/product")
    ResponseEntity<Response> deleteProduct(@RequestParam Integer productId);

    @PostMapping("/updateProduct")
    ResponseEntity<Response> updateProduct(@RequestBody ProductDTO productDTO, @RequestParam Integer id) throws IOException;

    @PostMapping("/addToFavorite")
    ResponseEntity<Response> addToFavorite(@RequestParam String email, @RequestParam Integer id) throws IOException;

    @DeleteMapping("/favorite")
    ResponseEntity<Response> deleteFromFavorite(@RequestParam String email, @RequestParam Integer id) throws IOException;

    @GetMapping("/favorite")
    ResponseEntity<Response> getFavorite(@RequestParam String email, @RequestParam Integer id) throws IOException;

    @GetMapping("/getAllProducts")
    ResponseEntity<PageableContent> getAllProducts(@RequestParam(required = false) String title,
                                                   @RequestParam(defaultValue = "0") int page,
                                                   @RequestParam(defaultValue = "6") int size) throws IOException;

    @GetMapping("/getUserProducts")
    ResponseEntity<List<ProductResponse>> getUserProducts(@RequestParam String userEmail) throws IOException;

    @GetMapping("/getUserFavoriteProducts")
    ResponseEntity<List<ProductResponse>> getUserFavoriteProducts(@RequestParam String userEmail) throws IOException;

    @GetMapping("/getProduct")
    ResponseEntity<ProductResp> getProduct(@RequestParam Integer productId) throws IOException;

    @GetMapping("/getTypes")
    ResponseEntity<List<TypeResponse>> getTypes() throws IOException;

    @GetMapping("/files/{filename:.+}")
    ResponseEntity<Resource> getFile(@PathVariable String filename, @RequestParam String filePath);

    @PostMapping("/requestBook")
    ResponseEntity<Response> requestBook(@RequestParam String userEmail, @RequestParam Integer id) throws IOException;

    @PostMapping("/myResponse")
    ResponseEntity<Response> responseToRequest(@RequestParam String userEmail, @RequestParam Integer id, @RequestParam String comment, @RequestParam Integer action) throws IOException;

    @GetMapping("/myRequests")
    ResponseEntity<List<RequestsResponse>> myRequests(@RequestParam String userEmail) throws IOException;

    @GetMapping("/myGivingRequests")
    ResponseEntity<List<RequestsResponse>> myGivingRequests(@RequestParam String userEmail) throws IOException;
}
