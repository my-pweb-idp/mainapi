package com.acs.pweb.config;

import com.acs.pweb.domain.response.EmailResponse;
import com.acs.pweb.email.EmailRequest;
//import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.IOException;

//@FeignClient(value = "EMAIL")
public interface EmailClient {
    @PostMapping(value = "/email/sendEmail",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<EmailResponse> sendEmail(@RequestBody EmailRequest emailRequest) throws IOException;

    
}
