package com.acs.pweb.email;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmailRequest {
    private List<String> emails;
    private List<String> ccEmails;
    private String templateName;
    private String contentType;
    private String subject;
    private Map<String, String> data;
}

