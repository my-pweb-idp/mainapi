package com.acs.pweb.service;

import com.acs.pweb.domain.Response;
import com.acs.pweb.domain.request.ProductDTO;
import com.acs.pweb.domain.response.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface ProductService {
    Response postImage(String email, String product, MultipartFile file);

    Response postProduct(ProductDTO productDTO);

    Response deleteProduct(Integer productId);

    Response updateProduct(ProductDTO productDTO, Integer id) throws IOException;

    Response addToFavorite(String email, Integer id) throws IOException;

    Response getFavorite(String email, Integer id) throws IOException;

    Response deleteFromFavorite(String email, Integer id) throws IOException;

    PageableContent getAllProducts(String title, int page, int size) throws IOException;

    List<ProductResponse> getUserProducts(String userEmail) throws IOException;

    List<ProductResponse> getUserFavoriteProducts(String userEmail) throws IOException;

    ProductResp getProduct(Integer productId) throws IOException;

    List<TypeResponse> getTypes() throws IOException;

    Response requestBook(String userEmail, Integer id);

    List<RequestsResponse> myRequests(String userEmail) throws IOException;

    List<RequestsResponse> myGivingRequests(String userEmail) throws IOException;

    Response responseToRequest(String userEmail, Integer id, String comment, Integer action);
}
