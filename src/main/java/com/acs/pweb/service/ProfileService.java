package com.acs.pweb.service;

import com.acs.pweb.domain.Response;
import com.acs.pweb.domain.request.ProductDTO;
import com.acs.pweb.domain.request.ProfileDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ProfileService {
    Response savaOrUpdateProfile(ProfileDTO profileDTO) throws IOException;

    Response updateProfile(ProfileDTO profileDTO) throws IOException;

    ProfileDTO getProfile(String email) throws IOException;
}
