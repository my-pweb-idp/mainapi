package com.acs.pweb.service;

import com.acs.pweb.email.EmailRequest;

import java.io.IOException;

public interface SendEmailService {
    void sendEmail(EmailRequest emailRequest) throws IOException;
}
