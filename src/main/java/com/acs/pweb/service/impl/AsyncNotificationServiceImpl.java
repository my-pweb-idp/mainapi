package com.acs.pweb.service.impl;

import com.acs.pweb.email.EmailRequest;
import com.acs.pweb.service.SendEmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
//import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class AsyncNotificationServiceImpl {

//    @Autowired
//    SendEmailService sendEmailService;

    @Value("${subject.name.changedInfoSubject}")
    String changedInfoSubject;

    @Autowired
    RabbitMQSender rabbitMQSender;

//    @Async
    public void sendEmailChangedInfo(String email, String nextEvalDate) throws IOException {

        EmailRequest emailRequest = new EmailRequest();

        emailRequest.setEmails(Collections.singletonList(email));
        emailRequest.setTemplateName("CHANGED_PROFILE_INFO");
        emailRequest.setContentType("html");
        emailRequest.setSubject(changedInfoSubject);

        Map<String, String> data = new HashMap<>();
        data.put("nextEvalDate", nextEvalDate);
        emailRequest.setData(data);
        log.info("Email is send to: " + emailRequest.getEmails());
        rabbitMQSender.send(emailRequest);
//        sendEmailService.sendEmail(emailRequest);
    }

//    @Async
    public void sendEmailCreateProfile(String email,String nextEvalDate) throws IOException {

        EmailRequest emailRequest = new EmailRequest();

        emailRequest.setEmails(Collections.singletonList(email));
        emailRequest.setTemplateName("ADD_BONUS_DAY");
        emailRequest.setContentType("html");
        emailRequest.setSubject(changedInfoSubject);

        Map<String, String> data = new HashMap<>();
        data.put("noOfBonusDays", nextEvalDate);
        emailRequest.setData(data);
        log.info("Email is send to: " + emailRequest.getEmails());
        rabbitMQSender.send(emailRequest);
//        sendEmailService.sendEmail(emailRequest);
    }
}