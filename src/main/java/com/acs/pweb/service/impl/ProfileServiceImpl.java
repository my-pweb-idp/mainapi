package com.acs.pweb.service.impl;

import com.acs.pweb.domain.Response;
import com.acs.pweb.domain.request.ProfileDTO;
import com.acs.pweb.model.Location;
import com.acs.pweb.model.Profile;
import com.acs.pweb.repository.LocationRepository;
import com.acs.pweb.repository.ProfileRepository;
import com.acs.pweb.service.ProfileService;
import org.springframework.stereotype.Service;
import java.io.IOException;

@Service
public class ProfileServiceImpl implements ProfileService {

    private final ProfileRepository profileRepository;
    private final LocationRepository locationRepository;
    private final AsyncNotificationServiceImpl asyncNotificationService;

    public ProfileServiceImpl(ProfileRepository profileRepository, LocationRepository locationRepository, AsyncNotificationServiceImpl asyncNotificationService) {
        this.profileRepository = profileRepository;
        this.locationRepository = locationRepository;
        this.asyncNotificationService = asyncNotificationService;
    }
    @Override
    public Response savaOrUpdateProfile(ProfileDTO profileDTO) throws IOException {
        Location location = new Location();
        Profile profile = profileRepository.getProfileByEmail(profileDTO.getEmail());

        if (profile == null) {
            profile = new Profile();
            profile.setEmail(profileDTO.getEmail());
            asyncNotificationService.sendEmailCreateProfile(profile.getEmail(),"ads");
        }

        if (profileDTO.getLocationLat() != null) {

            location.setLatitude(profileDTO.getLocationLat());
            location.setLongitude(profileDTO.getLocationLong());

            locationRepository.save(location);
            profile.setLocationId(location.getId());
        }


        if (profileDTO.getName() == null)
            profile.setName(profileDTO.getEmail().replaceAll("((@.*)|[^a-zA-Z])+", " ").trim());
        else {
            profile.setName(profileDTO.getName());
        }
        profile.setPhoto(profileDTO.getImagePath());


        profileRepository.save(profile);
        return new Response("User updated/saved", 200, profile.getId().toString());
    }

    @Override
    public Response updateProfile(ProfileDTO profileDTO) throws IOException {
        Location location = new Location();
        Profile profile = profileRepository.getProfileByEmail(profileDTO.getEmail());

        if (profile == null) {
            profile = new Profile();
            profile.setEmail(profileDTO.getEmail());
            asyncNotificationService.sendEmailCreateProfile(profile.getEmail(),"ads");
        }

        if (profileDTO.getLocationLat() != null) {

            location.setLatitude(profileDTO.getLocationLat());
            location.setLongitude(profileDTO.getLocationLong());

            locationRepository.save(location);
            profile.setLocationId(location.getId());
        }


        if (profileDTO.getName() == null)
            profile.setName(profileDTO.getEmail().replaceAll("((@.*)|[^a-zA-Z])+", " ").trim());
        else {
            profile.setName(profileDTO.getName());
        }
        profile.setPhoto(profileDTO.getImagePath());
        if (profileDTO.getPhoneNumber() != null) {
            profile.setPhoneNumber(profileDTO.getPhoneNumber());
        }

        profileRepository.save(profile);
        return new Response("User updated/saved", 200, profile.getId().toString());
    }

    @Override
    public ProfileDTO getProfile(String email) throws IOException {
        Profile profile = profileRepository.getProfileByEmail(email);
        ProfileDTO profileDTO = new ProfileDTO();
        profileDTO.setEmail(email);
        profileDTO.setPhoneNumber(profile.getPhoneNumber());
        profileDTO.setName(profile.getName());
        return profileDTO;
    }
}
