package com.acs.pweb.service.impl;

import com.acs.pweb.controller.ProductController;
import com.acs.pweb.domain.ImagePath;
import com.acs.pweb.domain.Response;
import com.acs.pweb.domain.request.ProductDTO;
import com.acs.pweb.domain.request.ProfileDTO;
import com.acs.pweb.domain.response.*;
import com.acs.pweb.model.*;
import com.acs.pweb.repository.*;
import com.acs.pweb.service.FilesStorageService;
import com.acs.pweb.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final FilesStorageService filesStorageService;
    private final ProductRepository productRepository;
    private final LocationRepository locationRepository;
    private final ProfileRepository profileRepository;
    private final TypeRepository typeRepository;
    private final FavoriteRepository favoriteRepository;
    private final RequestRepository requestRepository;
    private final StatusRepository statusRepository;
    private final AsyncNotificationServiceImpl asyncNotificationService;
    public static final String CREATED = "Created";

    @Override
    public Response postImage(String email, String product, MultipartFile file) {
        String message = "";
        try {
            List<String> fileNames = new ArrayList<>();
            String filePath = filesStorageService.initProduct(email, product);
            filesStorageService.save(file, filePath);
            fileNames.add(file.getOriginalFilename());
            message = "Uploaded the files successfully: " + fileNames;
            return new Response(message, 200, filePath);
        } catch (Exception e) {
            message = "Fail to upload files!";
            return new Response(message, 417, "");
        }
    }

    @Override
    public Response postProduct(ProductDTO productDTO) {
        Location location = new Location();

        location.setLatitude(productDTO.getLocationLat());
        location.setLongitude(productDTO.getLocationLong());

        locationRepository.save(location);

        Product product = new Product();

        product.setDescription(productDTO.getDescription());
        Profile profile = profileRepository.getProfileByEmail(productDTO.getEmail());
        product.setOwnerId(profile.getId());

        Type type = typeRepository.getTypeByName(productDTO.getTypeName());
        product.setTypeId(type.getId());
        product.setTitle(productDTO.getTitle());
        product.setImagePath("uploads/" + productDTO.getEmail() + "/" + productDTO.getImagePath());
        product.setAuthor(productDTO.getAuthor());

        product.setLocationId(location.getId());
        productRepository.save(product);
        return new Response(CREATED, 200, "");
    }

    @Override
    public Response deleteProduct(Integer productId) {
        Product product = productRepository.getById(productId);
        productRepository.delete(product);
        return new Response(CREATED, 200, "");
    }

    @Override
    public Response updateProduct(ProductDTO productDTO, Integer id) throws IOException {
        Product product = productRepository.getById(id);

        Location location = locationRepository.getById(product.getLocationId());

        location.setLatitude(productDTO.getLocationLat());
        location.setLongitude(productDTO.getLocationLong());

        locationRepository.save(location);

        product.setDescription(productDTO.getDescription());
        Profile profile = profileRepository.getProfileByEmail(productDTO.getEmail());
        product.setOwnerId(profile.getId());
        Type type = typeRepository.getTypeByName(productDTO.getTypeName());
        product.setTypeId(type.getId());
        product.setTitle(productDTO.getTitle());
//        product.setImagePath("uploads/" + productDTO.getEmail() + "/" + productDTO.getImagePath());
        product.setAuthor(productDTO.getAuthor());
        asyncNotificationService.sendEmailChangedInfo(profile.getEmail(), product.getTitle());
        product.setLocationId(location.getId());
        productRepository.save(product);
        return new Response(CREATED, 200, "");
    }

    @Override
    public Response addToFavorite(String email, Integer id) throws IOException {
        Profile profile = profileRepository.getProfileByEmail(email);
        Favorite favorite = new Favorite();
        favorite.setProductId(id);
        favorite.setProfileId(profile.getId());
        favoriteRepository.save(favorite);
        return new Response(CREATED, 200, "");
    }

    @Override
    public Response getFavorite(String email, Integer id) throws IOException {
        Profile profile = profileRepository.getProfileByEmail(email);
        Favorite favorite = favoriteRepository.getByProductIdAndProfileId(id, profile.getId());
        if (favorite != null)
            return new Response("Favorite", 200, "true");
        else return new Response("Favorite", 200, "false");
    }

    @Override
    public Response deleteFromFavorite(String email, Integer id) throws IOException {
        Profile profile = profileRepository.getProfileByEmail(email);
        Favorite favorite = favoriteRepository.getByProductIdAndProfileId(id, profile.getId());
        favoriteRepository.delete(favorite);
        return new Response("Deleted", 200, "");
    }

    @Override
    public PageableContent getAllProducts(String title, int page, int size) throws IOException {
        Pageable paging = PageRequest.of(page, size);
        Page<Product> products;
        if (title == null)
            products = productRepository.findAll(paging);
        else
            products = productRepository.findByTitleContaining(title, paging);
        List<ProductResponse> productResponses = new ArrayList<>();
        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();

        for (Product product : products.getContent()) {
            ProductResponse productResponse = new ProductResponse();
            productResponse.setId(product.getId());
            productResponse.setDescription(product.getDescription());
            productResponse.setTitle(product.getTitle());
            Resource[] resources = resourcePatternResolver.getResources("file:" + product.getImagePath() + "/*");
            String path = MvcUriComponentsBuilder
                    .fromMethodName(ProductController.class, "getFile", resources[0].getFilename(), product.getImagePath()).build().toString();
            productResponse.setImagePath(path);
            productResponses.add(productResponse);
        }
        return new PageableContent(productResponses, products.getNumber(), products.getTotalElements(), products.getTotalPages());
    }

    @Override
    public List<ProductResponse> getUserProducts(String userEmail) throws IOException {
        Profile profile = profileRepository.getProfileByEmail(userEmail);
        List<Product> products = productRepository.getAllByOwnerId(profile.getId());
        List<ProductResponse> productResponses = new ArrayList<>();
        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();

        for (Product product : products) {
            ProductResponse productResponse = new ProductResponse();
            productResponse.setId(product.getId());
            productResponse.setDescription(product.getDescription());
            productResponse.setTitle(product.getTitle());
            Resource[] resources = resourcePatternResolver.getResources("file:" + product.getImagePath() + "/*");
            String path = MvcUriComponentsBuilder
                    .fromMethodName(ProductController.class, "getFile", resources[0].getFilename(), product.getImagePath()).build().toString();
            productResponse.setImagePath(path);
            productResponses.add(productResponse);
        }
        return productResponses;
    }

    @Override
    public List<ProductResponse> getUserFavoriteProducts(String userEmail) throws IOException {
        Profile profile = profileRepository.getProfileByEmail(userEmail);

        List<Integer> productIds = favoriteRepository.getAllByProfileId(profile.getId());

        List<Product> products = productRepository.getAllByIdIn(productIds);
        List<ProductResponse> productResponses = new ArrayList<>();
        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();

        for (Product product : products) {
            ProductResponse productResponse = new ProductResponse();
            productResponse.setId(product.getId());
            productResponse.setDescription(product.getDescription());
            productResponse.setTitle(product.getTitle());
            Resource[] resources = resourcePatternResolver.getResources("file:" + product.getImagePath() + "/*");
            String path = MvcUriComponentsBuilder
                    .fromMethodName(ProductController.class, "getFile", resources[0].getFilename(), product.getImagePath()).build().toString();
            productResponse.setImagePath(path);
            productResponses.add(productResponse);
        }
        return productResponses;
    }

    @Override
    public ProductResp getProduct(Integer productId) throws IOException {
        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
        Product product = productRepository.getById(productId);

        Location location = locationRepository.getById(product.getLocationId());

        List<ImagePath> imagePaths = new ArrayList<>();
        Resource[] resources = resourcePatternResolver.getResources("file:" + product.getImagePath() + "/*");
        for (Resource resource : resources) {
            ImagePath imagePath = new ImagePath();
            String path = MvcUriComponentsBuilder
                    .fromMethodName(ProductController.class, "getFile", resource.getFilename(), product.getImagePath()).build().toString();
            imagePath.setOriginal(path);
            imagePath.setThumbnail(path);
            imagePaths.add(imagePath);
        }
        Profile profile = profileRepository.getById(product.getOwnerId());

        ProfileDTO profileDTO = new ProfileDTO();
        profileDTO.setEmail(profile.getEmail());
        profileDTO.setName(profile.getName());
        profileDTO.setPhoneNumber(profile.getPhoneNumber());

        ProductResp productResp = new ProductResp();

        productResp.setId(product.getId());
        productResp.setTitle(product.getTitle());
        productResp.setDescription(product.getDescription());
        productResp.setLocationLat(location.getLatitude());
        productResp.setLocationLong(location.getLongitude());
        productResp.setImagePaths(imagePaths);
        productResp.setProfile(profileDTO);
        productResp.setAuthor(product.getAuthor());
        productResp.setType(typeRepository.getById(product.getTypeId()).getName());

        return productResp;
    }

    @Override
    public List<TypeResponse> getTypes() throws IOException {
        List<Type> types = typeRepository.findAll();

        List<TypeResponse> typeResponses = new ArrayList<>();
        for (Type type : types) {
            TypeResponse typeResponse = new TypeResponse();
            typeResponse.setId(type.getId());
            typeResponse.setName(type.getName());
            typeResponse.setDescription(type.getDescription());
            typeResponses.add(typeResponse);
        }
        return typeResponses;
    }

    @Override
    public Response requestBook(String userEmail, Integer id) {
        Requests requests = new Requests();
        requests.setProductId(id);
        Profile userId = profileRepository.getProfileByEmail(userEmail);
        requests.setProfileId(userId.getId());
        requests.setStatusId(1);
        requestRepository.save(requests);
        return new Response("SUCCESS", 200, requests.getId().toString());
    }

    @Override
    public List<RequestsResponse> myRequests(String userEmail) throws IOException {
        Profile profile = profileRepository.getProfileByEmail(userEmail);
        List<Requests> requests = requestRepository.getAllByProfileId(profile.getId());
        List<RequestsResponse> requestsResponses = new ArrayList<>();

        for (Requests request : requests) {
            RequestsResponse response = new RequestsResponse();
            response.setStatus(statusRepository.getById(request.getStatusId()).getName());
            response.setTime(request.getCreationDate().toString());
            response.setProductResp(getProduct(request.getProductId()));
            requestsResponses.add(response);
        }
        return requestsResponses;
    }

    @Override
    public List<RequestsResponse> myGivingRequests(String userEmail) throws IOException {
        Profile profile = profileRepository.getProfileByEmail(userEmail);
        List<Requests> requests = requestRepository.getAllByBookProfileId(profile.getId());
        List<RequestsResponse> requestsResponses = new ArrayList<>();

        for (Requests request : requests) {
            RequestsResponse response = new RequestsResponse();
            response.setStatus(statusRepository.getById(request.getStatusId()).getName());
            response.setTime(request.getCreationDate().toString());
            response.setProductResp(getProductByProfileRequested(request.getProductId(), request.getProfileId()));
            requestsResponses.add(response);
        }
        return requestsResponses;
    }

    @Override
    public Response responseToRequest(String userEmail, Integer id, String comment, Integer action) {
        Profile profile = profileRepository.getProfileByEmail(userEmail);
        Requests requests = requestRepository.getByBookAndProfileId(id, profile.getId());
        requests.setStatusId(action);
        requests.setComment(comment);
        requestRepository.save(requests);
        return new Response("SUCCESS", 200, "SUCCESS");
    }


    public ProductResp getProductByProfileRequested(Integer productId, Integer profileId) throws IOException {
        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
        Product product = productRepository.getById(productId);

        Location location = locationRepository.getById(product.getLocationId());

        List<ImagePath> imagePaths = new ArrayList<>();
        Resource[] resources = resourcePatternResolver.getResources("file:" + product.getImagePath() + "/*");
        for (Resource resource : resources) {
            ImagePath imagePath = new ImagePath();
            String path = MvcUriComponentsBuilder
                    .fromMethodName(ProductController.class, "getFile", resource.getFilename(), product.getImagePath()).build().toString();
            imagePath.setOriginal(path);
            imagePath.setThumbnail(path);
            imagePaths.add(imagePath);
        }
        Profile profile = profileRepository.getById(profileId);

        ProfileDTO profileDTO = new ProfileDTO();
        profileDTO.setEmail(profile.getEmail());
        profileDTO.setName(profile.getName());

        ProductResp productResp = new ProductResp();

        productResp.setId(product.getId());
        productResp.setTitle(product.getTitle());
        productResp.setDescription(product.getDescription());
        productResp.setLocationLat(location.getLatitude());
        productResp.setLocationLong(location.getLongitude());
        productResp.setImagePaths(imagePaths);
        productResp.setProfile(profileDTO);
        productResp.setAuthor(product.getAuthor());
        productResp.setType(typeRepository.getById(product.getTypeId()).getName());
        return productResp;
    }
}
