package com.acs.pweb.service.impl;

import com.acs.pweb.config.EmailClient;
import com.acs.pweb.config.QueueSender;
import com.acs.pweb.email.EmailRequest;
import com.acs.pweb.service.SendEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class SendEmailServiceImpl implements SendEmailService {

//    @Autowired
//    EmailClient emailClient;

    @Autowired
    private QueueSender queueSender;

    @Override
    @Async
    public void sendEmail(EmailRequest emailRequest) throws IOException {
        try {
            queueSender.send("test message");
//            emailClient.sendEmail(emailRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
