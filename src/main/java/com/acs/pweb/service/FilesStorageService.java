package com.acs.pweb.service;

import java.nio.file.Path;
import java.util.stream.Stream;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FilesStorageService {
    public void init();
    public void save(MultipartFile file, String filePath);
    public Resource load(String filename, String filePath);
    public void deleteAll();
    public Stream<Path> loadAll(String filePath);
    public String initProfile(String profile);
    public String initProduct(String profile, String product);
}