package com.acs.pweb.domain.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.tomcat.util.buf.UEncoder;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProfileDTO {
    private String name;
    private String phoneNumber;
    private String email;
    private String imagePath;
    private String locationLat;
    private String locationLong;
}
