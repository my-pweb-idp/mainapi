package com.acs.pweb.domain.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductDTO {
    private String email;
    private String title;
    private String typeName;
    private String author;
    private String description;
    private String locationLat;
    private String locationLong;
    private String imagePath;
}
