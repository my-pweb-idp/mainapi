package com.acs.pweb.domain.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TypeResponse {
    private Integer id;
    private String name;
    private String description;

}
