package com.acs.pweb.domain.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestsResponse {
    private String time;
    private String status;
    private ProductResp productResp;
}
