package com.acs.pweb.domain.response;

import com.acs.pweb.domain.ImagePath;
import com.acs.pweb.domain.request.ProfileDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductResp {
    private Integer id;
    private String title;
    private String description;
    private String type;
    private List<ImagePath> imagePaths;
    private String locationLat;
    private String locationLong;
    private String author;
    private ProfileDTO profile;
}
