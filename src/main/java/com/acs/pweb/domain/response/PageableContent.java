package com.acs.pweb.domain.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageableContent {
    private List<ProductResponse> productResponse;
    private int currentPage;
    private long totalItems;
    private int totalPages;
}
