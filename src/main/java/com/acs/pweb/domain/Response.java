package com.acs.pweb.domain;

public class Response {

    private String message;
    private Integer status;
    private String result;

    public Response() {
    }

    public Response(Integer status, String result) {
        this.status = status;
        this.result = result;
    }

    public Response(String message, Integer status, String result) {
        this.message = message;
        this.status = status;
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "Response{" +
                "message='" + message + '\'' +
                ", status=" + status +
                ", messageStatus='" + result + '\'' +
                '}';
    }
}
