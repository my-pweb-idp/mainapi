package com.acs.pweb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "requests")
public class Requests {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "PRODUCT_ID")
    private Integer productId;

    @Column(name = "REQUEST_PROFILE_ID")
    private Integer profileId;

    @Column(name = "STATUS_ID")
    private Integer statusId;

    @Column(name = "COMMENT")
    private String comment;

    @Column(name = "SYS_CREATION_DATE", insertable = false)
    private Timestamp creationDate;

    @Column(name = "SYS_UPDATE_DATE", insertable = false)
    private Timestamp updateDate;
}
