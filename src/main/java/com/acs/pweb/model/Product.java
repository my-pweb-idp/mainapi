package com.acs.pweb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "product")
public class Product {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "OWNER_ID")
    private Integer ownerId;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "TYPE_ID")
    private Integer typeId;

    @Column(name = "LOCATION_ID")
    private Integer locationId;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "AUTHOR")
    private String author;

    @Column(name = "IMAGES_PATH")
    private String imagePath;
}
