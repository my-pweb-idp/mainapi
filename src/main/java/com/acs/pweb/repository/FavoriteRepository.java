package com.acs.pweb.repository;

import com.acs.pweb.model.Favorite;
import com.acs.pweb.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FavoriteRepository extends JpaRepository<Favorite, Integer> {
    @Query(value = "Select productId from favorite where profileId = :profileId", nativeQuery = true)
    List<Integer> getAllByProfileId(Integer profileId);

    Favorite getByProductIdAndProfileId(Integer productId, Integer profileId);
//    @Query("Select d from Divisions d where d.active=true and d.divisionName!=:defaultName")
//    List<Divisions> findAllByActiveTrue(String defaultName);
//
//    Optional<Divisions> findByDivisionName(String name);
//
//    @Query("SELECT distinct divisionId FROM ProfileDdt where departmentId = :departmentId")
//    Integer getDivisionIdForDep(Integer departmentId);
//
//    @Query("SELECT distinct p.divisionId FROM ProfileDdt p, Divisions d where d.id=d.divisionId and p.teamId = :teamId")
//    Integer getDivisionIdForTeam(Integer teamId);
}