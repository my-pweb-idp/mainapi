package com.acs.pweb.repository;

import com.acs.pweb.model.Product;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProductPaginationRepository extends PagingAndSortingRepository<Product, Integer> {
}