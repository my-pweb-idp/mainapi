package com.acs.pweb.repository;

import com.acs.pweb.model.Location;
import com.acs.pweb.model.Type;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeRepository extends JpaRepository<Type, Integer> {

    Type getTypeByName(String name);
//    @Query("Select d from Divisions d where d.active=true and d.divisionName!=:defaultName")
//    List<Divisions> findAllByActiveTrue(String defaultName);
//
//    Optional<Divisions> findByDivisionName(String name);
//
//    @Query("SELECT distinct divisionId FROM ProfileDdt where departmentId = :departmentId")
//    Integer getDivisionIdForDep(Integer departmentId);
//
//    @Query("SELECT distinct p.divisionId FROM ProfileDdt p, Divisions d where d.id=d.divisionId and p.teamId = :teamId")
//    Integer getDivisionIdForTeam(Integer teamId);
}