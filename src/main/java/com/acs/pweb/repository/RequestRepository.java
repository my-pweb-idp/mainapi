package com.acs.pweb.repository;

import com.acs.pweb.model.Requests;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestRepository extends JpaRepository<Requests, Integer> {
    @Query(value = "from requests where profileId = :profileId")
    List<Requests> getAllByProfileId(Integer profileId);

    @Query(value = "from requests where productId = :bookId and profileId = :profileId")
    Requests getByBookAndProfileId(Integer bookId, Integer profileId);

    @Query(value = "from requests where productId in (select id from product where ownerId = :profileId)")
    List<Requests> getAllByBookProfileId(Integer profileId);
}