package com.acs.pweb.repository;

import com.acs.pweb.model.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, Integer> {
    Profile getProfileByEmail(String email);
}