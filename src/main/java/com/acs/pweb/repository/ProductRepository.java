package com.acs.pweb.repository;

import com.acs.pweb.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    List<Product> getAllByOwnerId(Integer ownerId);

    @Query(value = "SELECT * FROM product where productId in :ids", nativeQuery = true)
    List<Product> getAllByIdIn(List<Integer> ids);

    Page<Product> findByTitleContaining(String title, Pageable pageable);
}