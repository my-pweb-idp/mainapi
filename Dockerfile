# FROM openjdk:16-jdk-alpine

# COPY . /usr/src/mainAPI
# # ARG envir=localhost
# # ENV envValue=$envir

# WORKDIR /usr/src/mainAPI

# RUN apk upgrade --update \
# 	&& apk add -U tzdata \
# 	&& cp /usr/share/zoneinfo/Europe/Bucharest /etc/localtime

# RUN ["chmod", "+x", "mainAPI-start.sh"]
# CMD ["echo", "AAAA"]
# CMD ["sh", "./mainAPI-start.sh"]

# # ENTRYPOINT ["./mainAPI-start.sh ${envValue}"]

# # FROM python:3.11-rc-slim

# # COPY requirements.txt /usr/src/app/
# # RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt

# # COPY *.py /usr/src/app/


# # EXPOSE 5000
# # EXPOSE 8000

# # CMD ["python3", "/usr/src/app/testapp.py"]
# # cd /usr/src/mainAPI

FROM maven:3.8.2-jdk-11-openj9

WORKDIR /usr/src/mainAPI

COPY . .

RUN mvn clean install

CMD mvn spring-boot:run